//components/Headers
import Head from 'next/head'
import "./Header.scss";

const Header = props =>{
  return (
    <div className="Header">
      <Head>
        <title>
          {props.appTitle}
        </title>
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width"
          key="viewport"/>
      </Head>
      <div className="Logo">{props.appTitle}</div>
    </div>
  );
} ;

export default Header;
