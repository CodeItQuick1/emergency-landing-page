// components/Highlights
import './Highlights.scss';

class Highlights extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className="Highlights">
        <div className="Item">
          <h2>What's needed to get a quote:</h2>
          <p>Not much of anything really.</p>
          <p>Will and determination</p>
          <p>A dash of boredom</p>
        </div>
        <div className="Item">
          <h2>Factors that affect the thing you receive:</h2>
          <p>
          Your boredom: the more bored you are the better.
          </p>
          <p>
          Your ability to thing.
          </p>
          <p>
          How often you've been doing things. Sharing is caring
          </p>
          <p>
          Your location: can't shovel snow in the desert.
          </p>
        </div>
      </div>
    )
  }
}

export default Highlights;
