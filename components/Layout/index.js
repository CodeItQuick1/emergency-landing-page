//components/Layout
import Head from "next/head";

import Header from "../Header";
import Footer from "../Footer";

import "./Layout.scss";
import "../index.scss";

const Layout = props => {
  const appTitle = "Special Rates on Insurance";

  return (
  <div className="Layout">
    <Head>
      <title>{appTitle}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
    </Head>

    <Header appTitle={appTitle} />
    <div className="Content">{props.children}</div>
    <Footer></Footer>
  </div>
  )
};

export default Layout;
