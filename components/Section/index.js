// components/Contact
import './Section.scss';

class Section extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="Section">
        <div className="Subtitle">{this.props.subtitle}</div>
        <h3>{this.props.title}</h3>
        <p>{this.props.abstract}</p>
        {this.props.children}
      </div>
    );
  }
}

export default Section
