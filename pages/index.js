// pages/index.js

import Layout from "../components/Layout";
import Section from "../components/Section";
import Contact from "../components/Contact";
import Highlights from "../components/Highlights"

const Index = () => {
  return (
    <Layout>
      <Section
        title="Do Things Now"
        subtitle="Are you doing things?"
        abstract="As of 2016 there were over 268 million registered things to do.">
      </Section>
      <Section
        subtitle="Free Things"
        title="Get a thing to do"
        abstract="Fill out the form below to be contacted by a professional delegator in your area.">
          <Highlights></Highlights>
      </Section>
      <Contact />
    </Layout>
  );
}

export default Index;
