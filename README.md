# Emergency Landing Page

This is a very quick and dirty landing page that I like to use to vet ideas and receive customer feedback.

## Tech

Cloud Resources are hosted on AWS using the Serverless Framework.

[serverless dot com](https://www.serverless.com/examples/)

[NextJS](https://nextjs.org/)

## Run

Project deployment depends on having a certificate known to the [Amazon Certificate Authority](https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-validate-dns.html) as well as a [Cloud Formation Origin Identity](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html).

Initialize project

`npm install`

Deploy AWS resources `npm run deploy:resources`

Typical work flow:

  1. `npm run dev`

  2. Edit file(s)

  3. `npm run deploy`

Remove AWS assets using Serverless:

  From the project root `serverless remove`
  _specify any stage with the `-s` flag_

NPM run Scripts

  To run the next server
      `npm run dev`

  Clean up previous builds
      `npm run clean`

  Build project using `next build`
      `npm run build`

  Export built project
      `npm run export`

  Start project with `next start`
      `npm start`

  Deploy AWS resources: typically only run once
      `npm run deploy:resources`

  Deploy NextJS static assets to s3:
      `npm run deploy:assets`

  Build, Export & Deploy Assets
      `npm run deploy`

Directory Contents:

  ```
   emergency-landing-page
   | .serverless              Packaged Cloud Formation Templates
   | components               React components
     | Widget                 Component root
       | Widget.scss          Component specific style
       | index.js             React component code
     index.scss               Style
   | out
   | pages                    NexJS pages convention
     | index.js               App root
   | theme
     | _colors.scss           Theme color
     | _fonts.scss            Theme fonts
     | _theme.scss            Theme root style
   | .gitignore
   | README.md                This file
   | next.config.js           NextJS config
   | package.json             NPM project config
   | serverless.yml           Serverless config
  ```


